package eprecise.libs.cdi.coroutines.scope.extension.deployment;

import eprecise.libs.cdi.coroutines.scope.CDICoroutineContext;
import eprecise.libs.cdi.coroutines.scope.CDICoroutineContextImpl;
import eprecise.libs.cdi.coroutines.scope.CoroutineScoped;
import io.quarkus.arc.deployment.ContextRegistrationPhaseBuildItem;
import io.quarkus.deployment.annotations.BuildStep;
import io.quarkus.deployment.builditem.FeatureBuildItem;

class CdiCoroutinesScopeExtensionProcessor {

    private static final String FEATURE = "cdi-coroutines-scope-extension";

    @BuildStep
    FeatureBuildItem feature() {
        return new FeatureBuildItem(FEATURE);
    }


    @BuildStep
    public ContextRegistrationPhaseBuildItem.ContextConfiguratorBuildItem transactionContext(ContextRegistrationPhaseBuildItem contextRegistrationPhase) {
        return new ContextRegistrationPhaseBuildItem.ContextConfiguratorBuildItem(contextRegistrationPhase.getContext()
                .configure(CoroutineScoped.class).normal().contextClass(CDICoroutineContextImpl.class));
    }
}
