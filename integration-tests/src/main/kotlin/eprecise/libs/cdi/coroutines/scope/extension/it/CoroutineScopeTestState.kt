package eprecise.libs.cdi.coroutines.scope.extension.it

import eprecise.libs.cdi.coroutines.scope.CoroutineScoped

@CoroutineScoped
open class CoroutineScopeTestState {
    var value = 0
}