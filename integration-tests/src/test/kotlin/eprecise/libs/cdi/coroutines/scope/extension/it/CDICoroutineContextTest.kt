package eprecise.libs.cdi.coroutines.scope.extension.it

import eprecise.libs.cdi.coroutines.scope.CDICoroutineContext
import io.quarkus.test.junit.QuarkusTest
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.RepeatedTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.util.concurrent.atomic.AtomicInteger
import javax.enterprise.inject.Instance
import javax.inject.Inject

@QuarkusTest
class CDICoroutineContextTest {

    @Inject
    lateinit var context: CDICoroutineContext

    @Inject
    lateinit var testStateProducer: Instance<CoroutineScopeTestState>

    private val testState get() = testStateProducer.get()

    @Test
    fun `must run single coroutine`() = runBlocking {
        var executed = false
        context.newContext {
            delay(50)
            executed = true
        }
        assertTrue(executed)
    }

    @RepeatedTest(10)
    fun `must run allow individual contexts`() = runBlocking {
        val executions = 100
        val count = AtomicInteger(0)
        (1..executions).map { i ->
            async {
                context.newContext {
                    delay(5)
                    assertEquals(0, testState.value)
                    testState.value = i
                    assertEquals(i, testState.value)
                    count.incrementAndGet()
                }
            }
        }.awaitAll()
        assertEquals(executions, count.get())
    }

    @Test
    fun `must be able to propagate exceptions`(): Unit = runBlocking {
        assertThrows<IllegalArgumentException> {
            context.newContext {
                delay(10)
                throw IllegalArgumentException()
            }
        }
    }


}

