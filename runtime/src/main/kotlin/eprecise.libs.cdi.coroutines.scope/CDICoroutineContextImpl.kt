package eprecise.libs.cdi.coroutines.scope

import io.quarkus.arc.ContextInstanceHandle
import io.quarkus.arc.InjectableBean
import io.quarkus.arc.InstanceHandle
import io.quarkus.arc.impl.ContextInstanceHandleImpl
import io.quarkus.arc.impl.CreationalContextImpl
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.asContextElement
import kotlinx.coroutines.cancel
import mu.KLogging
import javax.enterprise.context.ContextNotActiveException
import javax.enterprise.context.Dependent
import javax.enterprise.context.spi.Contextual
import javax.enterprise.context.spi.CreationalContext

@Dependent
class CDICoroutineContextImpl : CDICoroutineContext {

    private val currentContext get() = StoreContainer.store

    private val map get() = currentContext.get()?.map

    override val scope get() = this.currentContext.get()?.scope ?: throw ContextNotActiveException()

    override fun getScope() = CoroutineScoped::class.java

    override fun asContextElement() = currentContext.asContextElement()

    @Suppress("UNCHECKED_CAST")
    override fun <T> get(contextual: Contextual<T>, creationalContext: CreationalContext<T>?): T? {
        synchronized(currentContext) {
            val ctx = map ?: throw ContextNotActiveException()
            val instanceHandle = ctx[contextual]
            return if (instanceHandle != null) {
                instanceHandle.get() as T?
            } else {
                val context = creationalContext ?: CreationalContextImpl(contextual)
                val instance = contextual.create(context)
                ctx[contextual] = ContextInstanceHandleImpl(
                    contextual as InjectableBean<T>,
                    instance,
                    context
                )
                instance
            }
        }
    }

    override fun <T> get(contextual: Contextual<T>) = get(contextual, null)

    override fun isActive() = currentContext.get() != null

    override fun destroy(contextual: Contextual<*>) {
        synchronized(currentContext) {
            val ctx = map ?: return
            ctx.remove(contextual)?.destroy()
        }
    }

    override fun destroy() {
        deactivate()
    }

    override fun getState() = currentContext.get()

    override fun activate(scope: CoroutineScope) {
        synchronized(currentContext) {
            currentContext.set(StoreImpl(scope))
        }
    }

    override fun deactivate() {
        synchronized(currentContext) {
            val ctx = currentContext.get()?.map ?: return
            for ((contextual, instance) in ctx) {
                try {
                    instance.destroy()
                } catch (e: Exception) {
                    logger.warn { "Unable to destroy instance ${instance.get()} for bean: $contextual" }
                }
            }
            ctx.clear()
            currentContext.remove()
        }
    }

    data class StoreImpl(
        val scope: CoroutineScope,
        val map: MutableMap<Contextual<*>, InstanceHandle<*>> = mutableMapOf()
    ) : CDICoroutineContext.Store {
        override fun getContextualInstances() = map.values.associate { it.bean to it.get() }
    }

    object StoreContainer {
        val store = ThreadLocal<StoreImpl?>()
    }

    companion object : KLogging()
}
