package eprecise.libs.cdi.coroutines.scope

import io.quarkus.arc.InjectableContext
import kotlinx.coroutines.*
import kotlinx.coroutines.future.future


interface CDICoroutineContext : InjectableContext {

    val scope: CoroutineScope

    fun asContextElement(): ThreadContextElement<*>

    fun activate(scope: CoroutineScope)

    fun deactivate()

    suspend fun <T> newContext(action: suspend CoroutineScope.() -> T) = coroutineScope {
        try {
            activate(this)
            runWithResult(action)
        } finally {
            deactivate()
        }
    }

    fun <T> future(action: suspend CoroutineScope.() -> T) = scope.future(asContextElement()) { action() }

    fun launch(action: suspend CoroutineScope.() -> Unit) = scope.launch(asContextElement()) { action() }

    fun <T> async(action: suspend CoroutineScope.() -> T) = scope.async(asContextElement()) { action() }

    suspend fun run(action: suspend CoroutineScope.() -> Unit) = launch(action).join()

    suspend fun <T> runWithResult(action: suspend CoroutineScope.() -> T) = async(action).await()

    fun runBlocking(action: suspend CoroutineScope.() -> Unit) = runBlocking<Unit> { run(action) }

    interface Store : InjectableContext.ContextState

    companion object {
        val instance: CDICoroutineContext get() = CDICoroutineContextImpl()

        fun future(action: suspend CoroutineScope.() -> Unit) = instance.future(action)

        fun launch(action: suspend CoroutineScope.() -> Unit) = instance.launch(action)

        fun runBlocking(action: suspend CoroutineScope.() -> Unit) = instance.runBlocking(action)

        suspend fun run(action: suspend CoroutineScope.() -> Unit) = instance.run(action)

    }

}
