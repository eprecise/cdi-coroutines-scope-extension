package eprecise.libs.cdi.coroutines.scope

import java.lang.annotation.Inherited
import javax.enterprise.context.NormalScope
import kotlin.annotation.AnnotationRetention.*
import kotlin.annotation.AnnotationTarget.*

@NormalScope
@Retention(RUNTIME)
@Target(TYPE, CLASS, ANNOTATION_CLASS, FUNCTION, FIELD)
@MustBeDocumented
@Inherited
annotation class CoroutineScoped
